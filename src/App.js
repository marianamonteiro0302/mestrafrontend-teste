/* eslint-disable no-unused-vars */
import "./App.css";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import Footer from "./components/Footer";
import { BrowserRouter } from "react-router-dom";
import Routes from "./Routes";
import { ThemeProvider } from "@mui/material/styles";
import theme from "./theme/Theme";
import NewFooter from "./components/NewFooter/Newfooter";
import NewNavbar from "./components/NewNavbar/NewNavbar";


function App() {
  return (
    <ThemeProvider theme={theme}>
      <div className="App">
        <BrowserRouter>
          <Routes />
          {/* <NewFooter />  */}
        </BrowserRouter>
      </div>
    </ThemeProvider>
  );
}
export default App;
