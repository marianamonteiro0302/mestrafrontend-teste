import React, { useState } from "react";
import {
  Grid,
  Button,
  Typography,
  Divider,
  Container,
  Card,
  CardContent,
  TextField,
} from "@mui/material";
import { makeStyles } from "@material-ui/styles";
import Box from "@material-ui/core/Box";
import NewNavbar from "../components/NewNavbar/NewNavbar";
import theme from "../theme/Theme";
import InputMask from "react-input-mask";
import "../App.css";
import { width } from "@mui/system";
import { alignProperty } from "@mui/material/styles/cssUtils";
import { useTranslation } from "react-i18next";
import Swal from "sweetalert2";
import "../components/NewModal/newModal.css";
import Newfooter from "../components/NewFooter/Newfooter";
import WhatsAppIcon from "@mui/icons-material/WhatsApp";
import LocalPhoneIcon from "@mui/icons-material/LocalPhone";
import EmailIcon from "@mui/icons-material/Email";
import PrivatLogo from "../data/privatLogo.png";
import { useMediaQuery } from "@material-ui/core";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import axios from "axios";
import api from "../utils/axiosapi";

const useStyles = makeStyles({
  root: {
    width: "100%",
    height: "100vh",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    gap: "10px",
    //mobile
    [theme.breakpoints.down("sm")]: {
      margin: "80px 0px 80px 0px",
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "#EAEDED",
      },
    },
  },
  box: {
    width: "100%",
    display: "flex",
    justifyContent: "space-around",
    alignItems: "center",
    position: "relative",
  },
  rightDiv: {
    display: "flex",
    gap: "10px",
    //mobile
    [theme.breakpoints.down("md")]: {
      display: "none",
    },
  },
  formBoxContainer: {
    width: "100%",
    height: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    //mobile
    [theme.breakpoints.down("md")]: {
      alignItems: "center",
      justifyContent: "flex-start",
    },
  },
  cardContainer: {
    padding: "20px",
    display: "flex",
    flexDirection: "column",
    gap: "10px",
  },
  title: {
    display: "block",
    //mobile
    [theme.breakpoints.down("md")]: {
      display: "none",
    },
  },
});

const Contact = () => {
  const classes = useStyles();
  const { t, i18n } = useTranslation();
  const PrivatIconDesktop = useMediaQuery(theme.breakpoints.up("md"));
  const PrivatIconMobile = useMediaQuery(theme.breakpoints.down("sm"));
  const [nome, setNome] = useState("");
  const [sobrenome, setSobrenome] = useState("");
  const [telefone, setTelefone] = useState("");
  const [comentario, setComentario] = useState("");
  const [email, setEmail] = useState("");
  const showInMapClicked = () => {
    window.open(
      "https://www.google.com/maps/place/Mestra+Boats/@-22.3323056,-48.7627983,1069m/data=!3m1!1e3!4m14!1m8!3m7!1s0x0:0x0!2zMjLCsDE5JzU2LjMiUyA0OMKwNDUnNDEuMSJX!3b1!7e2!8m2!3d-22.3323184!4d-48.7614203!3m4!1s0x94bf5578da2609ab:0xcd23939e02babd7a!8m2!3d-22.3326358!4d-48.7615203"
    );
  };
  const handleSubmit = async () => {
    // console.log("enviou!");
    var url = "orc/contato";
    var data = {
      nome: nome,
      sobrenome: sobrenome,
      email: email,
      telefone: telefone,
      comentario: comentario,
    };
    await api.post(url, data).then((retorno) => {
      if (retorno.data.error) {
        Swal.fire({
          icon: "error",
          title: retorno.data.error,
          confirmButtonColor: theme.palette.primary.main,
        });
        return;
      }
      if (retorno.data) {
        Swal.fire({
          title: "Mensagem enviada!",
          text: "Agradecemos o contato, entraremos em contato em breve!",
          icon: "success",
          confirmButtonText: "OK",
          confirmButtonColor: theme.palette.primary.main,
        });
        return;
      }
      window.location.assign("/contact");
      // e.preventDefault();
    });
  };
  return (
    <>
      <div className="AppScrollbar">
        <NewNavbar position="initial" />
        <div className={classes.root}>
          <Box className={classes.box}>
            <div className={classes.title}>
              <Typography variant="h4">{t("contactPageTitle")}</Typography>
            </div>
            <div className={classes.rightDiv}>
              <div>
                <Typography
                  style={{ cursor: "pointer" }}
                  variant="body1"
                  onClick={() => {
                    window.location.href = "/";
                  }}
                >
                  Home
                </Typography>{" "}
              </div>
              <div>
                <Typography
                  variant="body1"
                  style={{ cursor: "pointer" }}
                  onClick={() => {
                    showInMapClicked();
                  }}
                >
                  Local
                </Typography>{" "}
              </div>
            </div>
          </Box>
          <Divider style={{ width: "60%", color: "#ececec" }} />
          <Box className={classes.formBoxContainer}>
            <div className={classes.cardContainer}>
              <div style={{ padding: "5px" }}>
                <Typography variant="h5">{t("contactUs")}</Typography>
                <Typography variant="body1">{t("preencherForm")}</Typography>
              </div>
              <Divider style={{ width: "100%", color: "#ececec" }} />
              <Grid container spacing={2}>
                <Grid item xs={12} md={6}>
                  <TextField
                    required
                    id="outlined-basic"
                    label={t("nome")}
                    onChange={(e) => setNome(e.target.value)}
                    variant="outlined"
                    fullWidth
                  />
                </Grid>
                <Grid item xs={12} md={6}>
                  <TextField
                    required
                    id="outlined-basic"
                    label={t("sobrenome")}
                    onChange={(e) => setSobrenome(e.target.value)}
                    variant="outlined"
                    fullWidth
                  />
                </Grid>
                <Grid item xs={12} md={6}>
                  <InputMask
                    mask="(99) 9999-99999"
                    // value={this.state.phone}
                    disabled={false}
                    maskChar=" "
                    onChange={(e) => setTelefone(e.target.value)}
                  >
                    {() => (
                      <TextField
                        fullWidth
                        required
                        id="outlined-basic"
                        label={t("telefone")}
                        variant="outlined"

                      // type="number"
                      />
                    )}
                  </InputMask>
                </Grid>
                <Grid item xs={12} md={6}>
                  <TextField
                    required
                    id="outlined-basic"
                    label="Email"
                    variant="outlined"
                    onChange={(e) => setEmail(e.target.value)}
                    fullWidth
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    required
                    multiline
                    rows={4}
                    id="outlined-basic"
                    label={t("mensagem")}
                    onChange={(e) => setComentario(e.target.value)}
                    variant="outlined"
                    fullWidth
                  />
                </Grid>
              </Grid>

              <Grid item xs={12}>
                {/* <Divider style={{width:"100%", marginTop:"5px"}} /> */}

                <Typography variant="body2" style={{ color: "gray" }}>
                  {/* * Caso queira solicitar um orçamento detalhado e customizado,
                  selecione o modelo desejado e entre em{" "}
                  <a href="/lanchas/mestra240">CUSTOMIZAR</a>. Complete as
                  etapas e envie seu modelo de Mestra desejado. */}
                  {t("detailRequest")}
                </Typography>
              </Grid>
              <Grid
                item
                xs={12}
                sx={{
                  display: "flex",
                  justifyContent: "flex-end",
                  alignItems: "flex-end",
                  padding: "10px",
                }}
              >
                <Button
                  style={{ width: "200px" }}
                  variant="outlined"
                  type="submit"
                  onClick={() => handleSubmit()}
                >
                  {t("send")}
                </Button>
              </Grid>

              <Grid container style={{ display: "flex", gap: "15px", }}>
                <Grid item xs={12} style={{ display: "flex", gap: "10px" }}>
                  <LocationOnIcon />
                  <Typography>
                    Av. Vírgilio Franceschi - 905 - Dist. Industrial -
                    Pederneiras-SP - CEP: 17280-000
                  </Typography>
                </Grid>
                <Grid item xs={12} style={{ display: "flex", gap: "10px" }}>
                  <WhatsAppIcon />
                  <Typography>+55 (14) 3252-2918</Typography>
                </Grid>
                <Grid item xs={12} style={{ display: "flex", gap: "10px" }}>
                  <LocalPhoneIcon />
                  <Typography>+55 (14) 99853-2123</Typography>
                </Grid>
                <Grid item xs={12} style={{ display: "flex", gap: "10px" }}>
                  <EmailIcon />
                  <Typography>example@gmail.com</Typography>
                </Grid>
              </Grid>
              {PrivatIconMobile ? (
                <div>
                  <img src={PrivatLogo} style={{ cursor: "pointer" }} />
                </div>
              ) : null}
            </div>
          </Box>
          {PrivatIconDesktop ? (
            <Grid
              container
              style={{
                width: "100%",
                display: "flex",
                justifyContent: "flex-end",
                gap: "15px",
              }}
            >
              <Grid
                item
                xs={6}
                style={{
                  display: "flex",
                  justifyContent: "flex-end",
                  gap: "15px",
                  padding: "50px",
                }}
              >
                <img src={PrivatLogo} style={{ cursor: "pointer" }} />
              </Grid>
            </Grid>
          ) : null}
        </div>
        <Newfooter contact />
      </div>
    </>
  );
};

export default Contact;
