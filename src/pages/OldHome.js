import React, { useState, useRef } from "react";
import NewNavbar from "../components/NewNavbar/NewNavbar";

import { ModelsWrapper, ModelSection } from "../components/OldHome";
import DefaultOverlayContent from "../components/NewHome/DefaultOverlayContent";
import UniqueOverlay from "../components/NewHome/UniqueOverlay";
import { Container, Spacer } from "../components/NewHome/HomeStyles";
import VideoContainer from "../components/Home/VideoSection/VideoContainer";

const NewHome = () => {
  const [modelURL, setModelURL] = useState([
    "mestra322",
    "mestra292",
    "mestra240",
    "mestra230",
    "mestra200",
    "mestra212",
    "mestra180",
    "mestra160",
  ]);
  return (
    <>
      <Container>
        <ModelsWrapper>
          <div>
            {[
              "Mestra 322",
              "Mestra 292",
              "Mestra 240",
              "Mestra 230",
              "Mestra 200",
              "Mestra 212",
              "Mestra 180",
              "Mestra 160",

            ].map((modelName) => (
              <ModelSection
                key={modelName}
                className="colored"
                modelName={modelName}
                overlayNode={
                  <DefaultOverlayContent
                    label={modelName}
                    modelNameURL={
                      modelName === "Mestra 322"
                        ? modelURL[0] :
                        modelName === "Mestra 292"
                          ? modelURL[1] :
                          modelName === "Mestra 240"
                            ? modelURL[2]
                            : modelName === "Mestra 230"
                              ? modelURL[3]
                              : modelName === "Mestra 200"
                                ? modelURL[4] :
                                modelName === "Mestra 212" ? modelURL[5]
                                  : modelName === "Mestra 180" ? modelURL[6] :
                                    modelName === "Mestra 160"
                                      ? modelURL[7]
                                      : null
                    }
                  //   description="Order Online for Delivery"
                  />
                }
              />
            ))}
          </div>
          <Spacer />
          {/* //Footer */}
          <UniqueOverlay />
        </ModelsWrapper>
      </Container >

    </>
  );
};

export default NewHome;
