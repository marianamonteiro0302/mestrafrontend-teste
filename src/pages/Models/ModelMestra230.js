import React from "react";

import NewModelPage from "../../components/NewModelPage/NewModelPage";
import mestra240Modelo from "../../data/images/desktop/home230_desktop.jpg";
import mestra240Mobile from "../../data/images/mobile/home230_nobile_v01.jpg";
import interior from "../../data/images/customLancha230/visao2Estofado.png";
import imgCarousel1 from "../../data/images/mestra230/carouselImg1.jpg";
import imgCarousel2 from "../../data/images/mestra230/carouselImg2.jpg";
import imgCarousel3 from "../../data/images/mestra230/carouselImg3.jpg";
import imgCarousel4 from "../../data/images/mestra230/carouselImg4.jpg";
import imgCarousel5 from "../../data/images/mestra230/carouselImg5.jpg";
import imgCarousel6 from "../../data/images/mestra230/carouselImg6.jpg";

import model from "../../data/images/customLancha230/fotoBasePNG.png";

import imgDetails1 from "../../data/images/details230_240_2.jfif";
import imgDetails2 from "../../data/images/details230_240.jfif";
import imgDetails3 from "../../data/images/image00018.jpeg";
import imgDetails4 from "../../data/images/image00029.jpeg";
import imgDetails5 from "../../data/images/image00013.jpeg";
import imgDetails6 from "../../data/images/image00009.jpeg";
import imgDetails7 from "../../data/images/image00017.jpeg";
import imgDetails8 from "../../data/images/image00014.jpeg";
import imgDetails9 from "../../data/images/image00011.jpeg";
import imgDetails10 from "../../data/images/image00016.jpeg";

const fichaProps = {
  mestra292: false,
  mestra322: false,
  mestra240: false,
  mestra230: true,
  mestra222: false,
  mestra212: false,
  mestra200: false,
  mestra198: false,
  mestra180: false,
  mestra160: false,
};

const imagens = [
  imgCarousel1,
  imgCarousel2,
  imgCarousel3,
  imgCarousel4,
  imgCarousel5,
  imgCarousel6
]

const ModelMestra230 = () => {
  return (
    <NewModelPage
      mestra240
      mestra230Text
      mestra230
      fichaProps={fichaProps}
      length="7,00 M"
      width="2,45 M"
      draught="0,36 M"
      img={mestra240Modelo}
      carouselImages={imagens}
      mobileImage={mestra240Mobile}
      imgInterior={interior}
      imgDetails1={imgDetails1}
      imgDetails2={imgDetails2}
      imgDetails3={imgDetails3}
      imgDetails4={imgDetails4}
      imgDetails5={imgDetails5}
      imgDetails6={imgDetails6}
      imgDetails7={imgDetails7}
      imgDetails8={imgDetails8}
      imgDetails9={imgDetails9}
      imgDetails10={imgDetails10}
      modelName="Mestra 230"
      modelURL="mestra230"
      model={model}
    />
  );
};

export default ModelMestra230;
