import styled from "styled-components";
//Images
import home322 from '../../data/images/MESTRA322/V3-Lancha-322-Home.png';
// import home292 from '../../data/images/MESTRA292/mestra292-home.png';
import home292 from '../../data/images/MESTRA292/V3-Lancha-292-Home.png';

import home240 from '../../data/images/desktop/home240_desktop.jpg';
import home230 from '../../data/images/desktop/home230_desktop.jpg';
import home200 from '../../data/images/desktop/home200_desktop.jpg';
import home212 from '../../data/images/desktop/home212_desktop.jpg';
import home160 from '../../data/images/desktop/home160_desktop.jpg';
import home180 from '../../data/images/desktop/home180_desktop.jpg';
//mobile
import home292Mobile from '../../data/images/mobile/Mobile-Lancha-292-Home.png';
import home322Mobile from '../../data/images/mobile/Mobile-Lancha-322-Home.png';
import home160Mobile from '../../data/images/mobile/home160_mobile_v01.jpg';
import home200Mobile from '../../data/images/mobile/home_200_mobile_v01.jpg';
import home212Mobile from '../../data/images/mobile/home212_mobile_v01.jpg';
import home230Mobile from '../../data/images/mobile/home230_nobile_v01.jpg';
import home240Mobile from '../../data/images/mobile/home240_mobile_v01.jpg';
import home180Mobile from '../../data/images/mobile/home180_mobile_v01.jpg';


export const Container = styled.div`
  height: 100vh;
  scroll-snap-type: y mandatory;
  overflow-y: scroll;
  position: relative;
  /* Remove default scrollbar styles */
  scrollbar-width: none; /* For Firefox */
  -ms-overflow-style: none; /* For Internet Explorer and Microsoft Edge */

  /* Hide scrollbar for modern web browsers */
  ::-webkit-scrollbar {
    display: none;
  }

  @media (max-width: 900px) {
    overflow: overlay;
    ::-webkit-scrollbar {
      background: transparent;
      width: 4px;
    }
    ::-webkit-scrollbar-thumb {
      background: transparent;
    }
  }
`;

// export const Wrapper = styled.div`
//   position: sticky;
//   top: 0;
//   left: 0;
//   right: 0;
//   bottom: 0;
// `;

export const Wrapper = styled.div`
  scroll-snap-type: y mandatory;
  overflow-y: scroll;
  height: 100vh;
  /* Add your custom scrollbar styles here for WebKit browsers */
`;

export const Spacer = styled.div`
  height: 15vh;
  background: white;
  }
`;
export const Content = styled.div`
  position: relative;
  height: 100vh;
  scroll-snap-align: start;
  display: flex;
  flex: 1 1 auto;
  flex-direction: column;
  margin-top: 16.5vh;



  :nth-child(1) {
    background-image: url(${home322});
    // background: linear-gradient(
    //     0deg,
    //     rgba(79, 129, 136, 0) 60%,
    //     rgba(131, 174, 188, 1) 100%
    //   ),
    //   url(${home322});
    background-size: cover;
    background-position: center center;
    @media (max-width: 1024px) {
      background-size: cover;
      background-repeat: no-repeat;
      background-image: url(${home322Mobile});
      background-position: center center;
    }
  }
  :nth-child(2) {
    background-image: url(${home292});
    // background: linear-gradient(
    //     0deg,
    //     rgba(79, 129, 136, 0) 60%,
    //     rgba(131, 174, 188, 1) 100%
    //   ),
    //   url(${home292});
    background-size: cover;
    background-position: center center;
    @media (max-width: 1024px) {
      background-size: cover;
      background-repeat: no-repeat;
      background-image: url(${home292Mobile});
      background-position: center center;
    }
  }
  :nth-child(3) {
    background-image: url(${home240});
    // background: linear-gradient(
    //     0deg,
    //     rgba(79, 129, 136, 0) 60%,
    //     rgba(131, 174, 188, 1) 100%
    //   ),
    //   url(${home240});
    background-size: cover;
    background-position: center center;
    @media (max-width: 1024px) {
      background-size: cover;
      background-repeat: no-repeat;
      background-image: url(${home240Mobile});
      background-position: center center;
    }
  }
  :nth-child(4) {
    background-position: center center;
    background-image: url(${home230});
    background-repeat: no-repeat;
    background-size: cover;
    @media (max-width: 1024px) {
      background-image: url(${home230Mobile});
      background-position: center center;
    }
  }
  :nth-child(5) {
    background-position: center center;
    background-image: url(${home200});
    background-repeat: no-repeat;
    background-size: cover;
    @media (max-width: 1024px) {
      background-image: url(${home200Mobile});
      background-position: center center;
    }
  }
  :nth-child(6) {
    background-position: center center;
    background-image: url(${home212});
    background-repeat: no-repeat;
    background-size: cover;
    @media (max-width: 1024px) {
      background-image: url(${home212Mobile});
      background-position: center center;
    }
  }
  :nth-child(7) {
    background-position: center center;
    background-image: url(${home180});
    background-repeat: no-repeat;
    background-size: cover;
    @media (max-width: 1024px) {
      background-image: url(${home180Mobile});
      background-position: center center;
    }
  }
  :nth-child(8) {
    background-position: center center;
    background: linear-gradient(
        180deg,
        rgba(215, 216, 210, 1) 0%,
        rgba(191, 195, 191, 0) 28%,
        rgba(173, 180, 178, 0) 48%,
        rgba(127, 141, 144, 0) 100%
      ),
      url(${home160});
    background-repeat: no-repeat;
    background-size: cover;
    @media (max-width: 1024px) {
      background-image: url(${home160Mobile});
      background-position: center center;
    }

  }
`;

export const Heading = styled.div`
margin-top: 16.5vh;
width: 100%;
text-align: center;
z-index: 11;
position: absolute;
> h1 {
  font-weight: 500;
  font-size: 40px;
  line-height: 48px;
  color: black;
 
}
> h2 {
  font-weight: normal;
  font-size: 14px;
  line-height: 20px;
  color: #5c5e62;
}
`;

export const Buttons = styled.div`
height: 100vh;
bottom: 0;
position: absolute;
align-items: flex-end;
display: flex;
gap: 20px;
margin-bottom: 100px;
z-index: 10;

> button {
  background-color: black;
  color: #fff;
  opacity: 0.85;
  width: 260px;
  height: 39.8px;
  font-size: 12px;
  font-weight: 500;
  letter-spacing: 0.4px;
  text-transform: uppercase;

  padding: 13px 40px;
  border-radius: 20px;
  border: none;
  outline: 0;
  cursor: pointer;

  &.white {
    background: white;
    color: #1a1720;
  //   opacity: 0.65;

  }

  // & + button {
  //   margin: 10px 0 0;
  // }
}


@media (max-width: 600px) {
  flex-direction: column;
  justify-content: flex-end;
  margin-bottom: 100px;
  gap: 1em;
  > button + button {
    margin: 5px 0 0px 0px;
  }
}
@media (min-height: 568px) and (max-height: 799px) {
  flex-direction: column;
  justify-content: flex-end;
  margin-bottom: 100px;
  gap: 1em;
  > button + button {
    margin: 5px 0 0px 0px;
    
  }
}
  @media only screen and (width:810px) and (height:1080px) and (orientation:portrait) {
    margin-bottom: 100px;
  }

    @media only screen and (width: 810px) and (height:1080px) and (orientation:landscape){
      flex-direction: column;
      margin-bottom: 100px;
      > button + button {
        margin: 0 0 0 10px;
      }
    }
   
  }
`;
export const Footer = styled.div`
  background-color: white;
  position: fixed;
  width: 100%;
  display: flex;
  justify-self: flex-end;
  justify-content: center;
  align-items: center;
  bottom: 0;
  left: 0;
  right: 0;
  
`;

export const ArrowDownDiv = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    padding-bottom: 20px;
    bottom: 20px;
`;

export const ArrowDownIcon = styled.div`
    animation: animateDown infinite 1.5s;
    height: 30px;
    width: 30px;
`;


export const ButtonArrow = styled.div`
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
`;

export const FooterDiv = styled.div`
width: '100%',
background-color: 'white',
display: 'flex',
justify-content: 'center',
align-items: 'center',
`;