import styled from "styled-components";

export const Footer = styled.footer`

background-color: ${({ backgroundColor }) =>
        backgroundColor || "transparent"};

width: 100%;
display: flex;
justify-content: center;
/* background-color: transparent; */
opacity: 0.5;
left: 0;
bottom: 0;
height: 50px;
align-items: center;

`