import React from "react";
import "./footer.css";
import { Footer } from "./footerstyled";
import { Link } from "react-router-dom";


const FooterComponent = () => {
  return (
    <footer className="footer">
      <ul>
        <Link
          style={{ color: "inherit" }}
          to="/about"
          onClick={() => {
            window.location.href = "/about";
          }}
        >
          {" "}
          <li>Mestra © 202ee1</li>{" "}
        </Link>
        <Link
          style={{ color: "inherit" }}
          to="/contact"
          onClick={() => {
            window.location.href = "/contact";
          }}
        >
          <li>Contato</li>{" "}
        </Link>
        <Link
          style={{ color: "inherit" }}
          to="/about"
          onClick={() => {
            window.location.href = "/about";
          }}
        >
          {" "}
          <li>Empresa</li>{" "}
        </Link>
        <li>Local</li>
      </ul>
    </footer>
  );
};
export default FooterComponent;
