/* eslint-disable jsx-a11y/alt-text */
import { React, useState, useEffect } from "react";
import NewNavbar from "../NewNavbar/NewNavbar";
import "./modelPage.css";
import { BsArrowLeftSquareFill } from "react-icons/bs";
import { BsArrowRightSquareFill } from "react-icons/bs";
import { MotionAnimate } from "react-motion-animate";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import MercuryEngine from "../../data/images/MercuryEngine.jpg";
import { BiLoader } from "react-icons/bi";
import "../Styles/customPage.css";
import theme from "../../theme/Theme";
import { Typography } from "@mui/material";
import { Grid, Button, Box } from "@mui/material";

const ModelPage = (props) => {
  const [loaded, setLoaded] = useState(false);
  const { t, i18n } = useTranslation();
  const CarouselImg = [
    props.carouselImg1,
    props.carouselImg2,
    props.carouselImg3,
    props.carouselImg4,
    props.carouselImg5,
    props.carouselImg6,
  ];

  const [currentImg, setCurrentImg] = useState(0);
  const arrowImg = (value) => {
    if (value === "right") {
      setCurrentImg(1);
      if (currentImg === 1) {
        setCurrentImg(2);
      }
      if (currentImg === 2) {
        setCurrentImg(3);
      }
      if (currentImg === 3) {
        setCurrentImg(4);
      }
      if (currentImg === 4) {
        setCurrentImg(5);
      }
      if (currentImg === 0) {
        setCurrentImg(1);
      }
    } else if (value === "left") {
      setCurrentImg(5);
      if (currentImg === 5) {
        setCurrentImg(4);
      }
      if (currentImg === 4) {
        setCurrentImg(3);
      }
      if (currentImg === 3) {
        setCurrentImg(2);
      }
      if (currentImg === 2) {
        setCurrentImg(1);
      }
      if (currentImg === 1) {
        setCurrentImg(0);
      }
    }
  };
  return (
    // TOPO DA PÁGINA

    <div className="App">
      <NewNavbar backgroundColor="transparent" />
      <div className="containerModel">
        <div className="contentModel">
          <div className="contentImg">
            <img src={props.img} />
          </div>
          <div className="subtitleTitleContainer">
            <h1 style={{ color: "white" }}>
              {props.title} {props.subtitle}
            </h1>
          </div>

          {/* <div
            style={{
              width: "fit-content",
              height: "fit-content",
              zIndex: "0",
              backgroundImage: `url(${props.img})`,
              backgroundPosition: "center",
              backgroundSize: "cover",
              backgroundRepeat: "no-repeat",
              position: "relative",
            }}
          ></div> */}
          <div className="detailsContainer">
            <div className="detailsContent">
              <h2 style={{ color: "white" }}>{props.boca}</h2>
              <p style={{ color: "white" }}>{t("boca")}</p>
            </div>
            <div className="detailsContent">
              <h2 style={{ color: "white" }}>{props.calado}</h2>
              <p style={{ color: "white" }}>{t("calado")}</p>
            </div>
            <div className="detailsContent">
              <h2 style={{ color: "white" }}>{props.comprimento}</h2>
              <p style={{ color: "white" }}>{t("comprimento")}</p>
            </div>
            <div className="detailsContent">
              {props.mestra240 && (
                <Link
                  className="navLinkStyle"
                  onClick={() => {
                    window.location.href = "/lanchas/mestra240";
                  }}
                  to="/lanchas/mestra240"
                  activeStyle
                >
                  <button className="modelButton">
                    {t("solicitarOrcamento")}
                  </button>
                </Link>
              )}
              {props.mestra230 && (
                <Link
                  className="navLinkStyle"
                  onClick={() => {
                    window.location.href = "/lanchas/mestra230";
                  }}
                  to="/lanchas/mestra230"
                  activeStyle
                >
                  <button className="modelButton">
                    {t("solicitarOrcamento")}
                  </button>
                </Link>
              )}
              {props.mestra222 && (
                <Link
                  className="navLinkStyle"
                  Click={() => {
                    window.location.href = "/lanchas/mestra222";
                  }}
                  to="/lanchas/mestra222"
                  activeStyle
                >
                  <button className="modelButton">
                    {t("solicitarOrcamento")}
                  </button>
                </Link>
              )}
              {props.mestra212 && (
                <Link
                  onClick={() => {
                    window.location.href = "/lanchas/mestra212";
                  }}
                  to="/lanchas/mestra212"
                  activeStyle
                >
                  <button className="modelButton">
                    {t("solicitarOrcamento")}
                  </button>
                </Link>
              )}
              {props.mestra200 && (
                <Link
                  onClick={() => {
                    window.location.href = "/lanchas/mestra200";
                  }}
                  to="/lanchas/mestra200"
                  activeStyle
                >
                  <button className="modelButton">
                    {t("solicitarOrcamento")}
                  </button>
                </Link>
              )}
              {props.mestra198 && (
                <Link
                  onClick={() => {
                    window.location.href = "/lanchas/mestra198";
                  }}
                  to="/lanchas/mestra198"
                  activeStyle
                >
                  <button className="modelButton">
                    {t("solicitarOrcamento")}
                  </button>
                </Link>
              )}
              {props.mestra180 && (
                <Link
                  onClick={() => {
                    window.location.href = "/lanchas/mestra180";
                  }}
                  to="/lanchas/mestra180"
                  activeStyle
                >
                  <button className="modelButton">
                    {t("solicitarOrcamento")}
                  </button>
                </Link>
              )}
              {props.mestra160 && (
                <Link
                  onClick={() => {
                    window.location.href = "/lanchas/mestra160";
                  }}
                  to="/lanchas/mestra160"
                  activeStyle
                >
                  <button className="modelButton">
                    {t("solicitarOrcamento")}
                  </button>
                </Link>
              )}
            </div>
          </div>
        </div>
      </div>
      {/* INTERIOR */}{" "}
      <div className="interiorContainer">
        <div className="interiorTitle">
          {" "}
          <h2>{t("interiorPersonalizavel")}</h2>{" "}
        </div>
        <div
          style={{
            width: "100%",
            height: "100vh",
            backgroundImage: `url(${props.imgInterior})`,
            position: "relative",
            backgroundSize: "cover",
            backgroundPosition: "center",
            backgroundRepeat: "no-repeat",
            zIndex: "0",
          }}
        ></div>
        <div className="maskInterior"></div>
        <div className="coresContainer">
          {" "}
          <div className="cores">
            <p>
              {props.colorOptions} {t("colorOptions")}
            </p>
            <p>{t("cascoCores")}</p>

            <div className="colorOptions">
              <div
                style={{
                  display: "flex",
                  zIndex: "1",
                  position: "relative",
                }}
              >
                {props.gray && <div className="colorGrayOption" />}
                {props.mestra160 && <div className="colorRedOption" />}
              </div>
              <div
                style={{
                  display: "flex",
                  zIndex: "3",
                  position: "relative",
                }}
              >
                {props.white && <div className="colorWhiteOption"></div>}
              </div>
              <div
                style={{
                  display: "flex",
                  zIndex: "2",
                  position: "absolute",
                }}
              >
                {props.black && <div className="colorBlackOption"></div>}
              </div>
            </div>
          </div>
        </div>
        <div className="coresRightContainer">
          <div className="coresRight">
            <p>
              {props.colorOptionsEstofado} {t("colorOptions")}
            </p>
            <p> {t("coresEstofado")}</p>
            <div className="colorOptions">
              <div
                style={{
                  display: "flex",
                  zIndex: "1",
                  position: "relative",
                }}
              >
                {" "}
                <div className="colorGrayOption"></div>{" "}
              </div>
              <div
                style={{
                  display: "flex",
                  zIndex: "3",
                  position: "relative",
                }}
              >
                {" "}
                <div className="colorWhiteEstofadoOption"></div>{" "}
              </div>
              <div
                style={{
                  display: "flex",
                  zIndex: "2",
                  position: "absolute",
                }}
              >
                {" "}
                {props.blue && <div className="colorBlueOption"></div>}
                {props.mestra160 && <div className="terracottaInterior" />}
              </div>
            </div>
          </div>
        </div>
        <div className="coresCenterContainer">
          <div className="coresCenter">
            <p>
              {props.colorOptionsTapecaria} {t("colorOptions")}
            </p>
            <p>{t("coresTapecaria")}</p>
            <div className="colorOptions">
              <div
                style={{
                  display: "flex",
                  zIndex: "1",
                  position: "relative",
                }}
              >
                {" "}
                <div className="colorBegeOption"></div>{" "}
              </div>
              <div
                style={{
                  display: "flex",
                  zIndex: "3",
                  position: "relative",
                }}
              >
                {" "}
                <div className="colorCarameloOption"></div>{" "}
              </div>
              <div
                style={{
                  display: "flex",
                  zIndex: "2",
                  position: "absolute",
                }}
              >
                {" "}
                <div className="colorTerracottaOption"></div>{" "}
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* CAROUSEL */}
      <div
        style={{
          display: "flex",
          alignItems: "center",
          backgroundColor: "black",
          color: "white",
          boxSizing: "border-box",
          width: "100vw",
          height: "100vh",
          justifyContent: "center",
        }}
      >
        {" "}
        {loaded ? null : (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100%",
              width: "100%",
            }}
          >
            <BiLoader
              className="loadIcon"
              style={{ width: "50px", height: "50px" }}
            />
          </div>
        )}
        <div className="carouselModel">
          <div className="arrowsModel">
            <BsArrowLeftSquareFill
              onClick={() => arrowImg("left")}
              style={{
                width: "30px",
                height: "30px",
                cursor: "pointer",
                backgroundColor: "white",
                border: "1px solid white",
              }}
            />
            <BsArrowRightSquareFill
              onClick={() => arrowImg("right")}
              style={{
                width: "30px",
                height: "30px",
                cursor: "pointer",
                backgroundColor: "white",
                border: "1px solid white",
              }}
            />
          </div>
          <div
            className="carouselImgModel"
            style={{ width: "100%", height: "100%" }}
          >
            <img
              onLoad={() => setLoaded(true)}
              src={CarouselImg[currentImg]}
            ></img>
          </div>
        </div>
      </div>
      {/* DETALHES */}
      <Box
        p={20}
        style={{
          backgroundColor: "black",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Grid container xs={12}>
          <Grid
            item
            xs={6}
            style={{ display: "flex", justifyContent: "flex-end" }}
          >
            <img src={props.img1} style={{ width: "516px" }} />
          </Grid>
          <Grid
            container
            item
            xs={6}
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Grid item xs={4}>
              <Typography color="white">
                Up to 10 teraflops of processing power enables in-car gaming
                on-par with today’s newest consoles. Wireless controller
                compatibility lets you game from any seat.
              </Typography>
            </Grid>
          </Grid>
          <Grid
            container
            item
            xs={6}
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              backgroundColor:"green"
            }}
          >
            <Grid
              item
              //xs={4}
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                backgroundColor:"pink",
                padding:"20px",
                width:"50%",
              }}
            >
              <Typography color="white">
                Up to 10 teraflops of processing power enables in-car gaming
                on-par with today’s newest consoles. Wireless controller
                compatibility lets you game from any seat.
              </Typography>
            </Grid>
          </Grid>
          <Grid
            item
            xs={6}
            style={{ display: "flex", justifyContent: "flex-start" }}
          >
            <img src={props.img2} style={{ width: "516px" }} />
          </Grid>
        </Grid>
      </Box>
      {/* MOTOR  */}
      <div className="containerModel" style={{ height: "80vh" }}>
        <img
          style={{
            width: "100%",
            height: "80vh",
            zIndex: "0",
            position: "relative",
            filter: "alpha(opacity=50)",
          }}
          src={MercuryEngine}
        />
        <div className="mask"></div>
        <div
          className="contentModel"
          style={{
            zIndex: "1",
            position: "absolute",
            justifyContent: "center",
          }}
        >
          {" "}
          <MotionAnimate animation="fadeInUp" reset={true}>
            <h2
              style={{ fontSize: "40px", color: "white", textAlign: "center" }}
            >
              {t("mercuryMotor")}
            </h2>
            <p style={{ color: "white", textAlign: "center" }}>
              {t("mercuryMotorIdeal")}
            </p>{" "}
          </MotionAnimate>
        </div>
      </div>
      <div className="lastSection">
        <div className="lastDiv">
          {" "}
          <p>{props.title}</p>
          {props.mestra240 && (
            <Link
              className="navLinkStyle"
              onClick={() => {
                window.location.href = "/lanchas/mestra240";
              }}
              to="/lanchas/mestra240"
              activeStyle
            >
              <button id="lastButton" className="modelButton">
                {" "}
                {t("peçaAgora")}
              </button>
            </Link>
          )}
          {props.mestra230 && (
            <Link
              className="navLinkStyle"
              onClick={() => {
                window.location.href = "/lanchas/mestra230";
              }}
              to="/lanchas/mestra230"
              activeStyle
            >
              <button id="lastButton" className="modelButton">
                {" "}
                {t("peçaAgora")}
              </button>
            </Link>
          )}
          {props.mestra222 && (
            <Link
              className="navLinkStyle"
              onClick={() => {
                window.location.href = "/lanchas/mestra222";
              }}
              to="/lanchas/mestra222"
              activeStyle
            >
              <button id="lastButton" className="modelButton">
                {" "}
                {t("peçaAgora")}
              </button>
            </Link>
          )}
          {props.mestra212 && (
            <Link
              className="navLinkStyle"
              onClick={() => {
                window.location.href = "/lanchas/mestra212";
              }}
              to="/lanchas/mestra212"
              activeStyle
            >
              <button id="lastButton" className="modelButton">
                {" "}
                {t("peçaAgora")}
              </button>
            </Link>
          )}
          {props.mestra200 && (
            <Link
              className="navLinkStyle"
              onClick={() => {
                window.location.href = "/lanchas/mestra200";
              }}
              to="/lanchas/mestra200"
              activeStyle
            >
              <button id="lastButton" className="modelButton">
                {" "}
                {t("peçaAgora")}
              </button>
            </Link>
          )}
          {props.mestra198 && (
            <Link
              className="navLinkStyle"
              onClick={() => {
                window.location.href = "/lanchas/mestra198";
              }}
              to="/lanchas/mestra198"
              activeStyle
            >
              <button id="lastButton" className="modelButton">
                {" "}
                {t("peçaAgora")}
              </button>
            </Link>
          )}
          {props.mestra180 && (
            <Link
              className="navLinkStyle"
              onClick={() => {
                window.location.href = "/lanchas/mestra180";
              }}
              to="/lanchas/mestra180"
              activeStyle
            >
              <button id="lastButton" className="modelButton">
                {" "}
                {t("peçaAgora")}
              </button>
            </Link>
          )}
          {props.mestra160 && (
            <Link
              className="navLinkStyle"
              onClick={() => {
                window.location.href = "/lanchas/mestra160";
              }}
              to="/lanchas/mestra160"
              activeStyle
            >
              <button id="lastButton" className="modelButton">
                {" "}
                {t("peçaAgora")}
              </button>
            </Link>
          )}
        </div>
        <img className="lastImg" src={props.img}></img>
      </div>
    </div>
  );
};

export default ModelPage;
