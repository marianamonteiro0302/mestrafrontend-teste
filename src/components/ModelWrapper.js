import React from 'react';
import { useTranslation } from 'react-i18next';
// import { Container, Heading, Buttons } from './NewHome/styles';
import styled from "styled-components";
import { makeStyles } from '@material-ui/core';

export const Container = styled.div`
  display: flex;
  position: sticky;
  // height:100vh;
  z-index:1000;
  flex-direction: column;
  justify-content: space-between;
  flex: 1;
  @media (max-width: 900px) {
  padding-bottom: 20px;
  }
`;

export const Heading = styled.div`
  margin-top: 16.5vh;

  width: 100%;
  text-align: center;

  > h1 {
    font-weight: 500;
    font-size: 40px;
    line-height: 48px;
    color: black;
  }
  > h2 {
    font-weight: normal;
    font-size: 14px;
    line-height: 20px;
    color: #5c5e62;
  }
`;

export const Buttons = styled.div`
  display: flex;
  // flex-direction: column;
  justify-content: center;
  align-items: center;
  gap: 10px;
  margin-bottom: 100px;
  z-index: 10;
  > button {
    background-color: black;
    color: #fff;
    opacity: 100%;
    width: 260px;
    font-size: 12px;
    font-weight: 500;
    letter-spacing: 0.4px;
    text-transform: uppercase;

    padding: 13px 40px;
    border-radius: 20px;
    border: none;
    outline: 0;
    cursor: pointer;

    &.white {
      background: white;
      color: #1a1720;
    //   opacity: 0.65;

    }

    // & + button {
    //   margin: 10px 0 0;
    // }
  }

  @media (max-width: 600px) {
    flex-direction: column;
    margin-bottom: 80px;

    > button + button {
      margin: 10px 0 0 0px;
    }
  }
  @media (max-width: 380px) {
    flex-direction: column;
    margin-bottom: 80px;

    > button + button {
      margin: 5px 0 0 10px;
    }
  }
    @media only screen and (width:810px) and (height:1080px) and (orientation:portrait) {
      margin-bottom: 100px;
    }
 
      @media only screen and (width: 810px) and (height:1080px) and (orientation:landscape){
        flex-direction: column;
        margin-bottom: 100px;
        > button + button {
          margin: 0 0 0 10px;
        }
      }
     
    }
`;


const useStyles = makeStyles({
    arrowDownDiv: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        paddingBottom: '20px',
        bottom: '20px',
    },

    arrowDownIcon: {
        animation: 'animateDown infinite 1.5s',
        height: '30px',
        width: '30px',
    },
    buttonArrow: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
});
const ModelWrapper = ({
    label,
    lanchaNome
}) => {
    const classes = useStyles();
    const { t, i18n } = useTranslation();
    return (
        <Container>
            <Heading>
                <h1
                    style={{ fontWeight: 'bold', cursor: 'pointer' }}
                    onClick={() => (window.location.href = `/lanchas/${lanchaNome}`)}
                >
                    {label}
                </h1>
            </Heading>
            <div className={classes.buttonArrow}>
                <Buttons>
                    {lanchaNome === 'mestra322' || lanchaNome === 'mestra292' ? (
                        <button
                            onClick={() => (window.location.href = `/models/${lanchaNome}`)}
                        >
                            Model Page
                        </button>
                    ) : (
                        <button
                            onClick={() =>
                                (window.location.href = `/lanchas/${lanchaNome}`)
                            }
                        >
                            {' '}
                            {t('customizar')}
                        </button>
                    )}
                    <button
                        className="white"
                        onClick={() => (window.location.href = `/contact`)}
                    >
                        {t('solicitarOrcamento')}
                    </button>
                </Buttons>
                {/* {label === "Mestra 240" ? (
          <div className={classes.arrowDownDiv}>
            <IoIosArrowDown className={classes.arrowDownIcon} />
          </div>
        ) : null} */}
            </div>
        </Container>
    );
};

export default ModelWrapper;
