/* eslint-disable jsx-a11y/alt-text */
import { React, useState, useRef } from "react";
import {
  Nav,
  NavLink,
  Bars,
  NavMenu,
  NavRight,
  NavLogo,
} from "./NavbarElements";
import "semantic-ui-css/semantic.min.css";
import "../Styles/home.css";
import { useTranslation } from "react-i18next";
import logo from "../../data/images/logo.jpg";
import "./navMobile.css";
import { Link } from "react-router-dom";
import brFlag from "../../data/images/flags/br.svg";
import usFlag from "../../data/images/flags/us.svg";

const PtButton = ({ language }) => {
  return (
    <button
      onClick={language}
      style={{ border: "none", background: "transparent" }}
    >
      <img style={{ width: "20px", height: "20px" }} src={brFlag} />
    </button>
  );
};
const EnButton = ({ language }) => {
  return (
    <button
      onClick={language}
      style={{ border: "none", background: "transparent" }}
    >
      <img style={{ width: "20px", height: "20px" }} src={usFlag} />
    </button>
  );
};

const NavMenus = () => {
  return (
    <div style={{display:"flex", justifyContent:"center", alignItems:"center", justifySelf:"center", marginRight:"50px"}}>
      <NavLink
        onClick={() => {
          window.location.href = "/models/mestra240";
        }}
        to="/models/mestra240"
        activeStyle
      >
        M240
      </NavLink>
      <NavLink
        onClick={() => {
          window.location.href = "/models/mestra230";
        }}
        to="/models/mestra230"
        activeStyle
      >
        M230
      </NavLink>
      <NavLink
        onClick={() => {
          window.location.href = "/models/mestra222";
        }}
        to="/models/mestra222"
        activeStyle
      >
        M222
      </NavLink>
      <NavLink
        onClick={() => {
          window.location.href = "/models/mestra212";
        }}
        to="/models/mestra212"
        activeStyle
      >
        M212
      </NavLink>
      <div
        style={{
          display: "flex",
          marginTop: "12px",
          flexDirection: "column",
          textAlign: "center",
        }}
      >
        <NavLink
          onClick={() => {
            window.location.href = "/models/mestra200";
          }}
          to="/models/mestra200"
          activeStyle
        >
          M200
        </NavLink>
        <div className="lancamento">
          <p>Lançamento</p>
        </div>
      </div>
      <NavLink
        onClick={() => {
          window.location.href = "/models/mestra198";
        }}
        to="/models/mestra198"
        activeStyle
      >
        M198
      </NavLink>
      <NavLink
        onClick={() => {
          window.location.href = "/models/mestra180";
        }}
        to="/models/mestra180"
        activeStyle
      >
        M180
      </NavLink>
      <NavLink
        onClick={() => {
          window.location.href = "/models/mestra160";
        }}
        to="/models/mestra160"
        activeStyle
      >
        M160
      </NavLink>
    </div>
  );
};
const Navbar = (props) => {
  const [sidebar, setSidebar] = useState(false);
  const [menuSidebar, setMenuSidebar] = useState(false);
  const showMenuSidebar = () => setMenuSidebar(!menuSidebar);
  const showSidebar = () => setSidebar(!sidebar);

  const { t, i18n } = useTranslation();
  const changeLanguage = (lng) => {
    i18n.changeLanguage(lng);
  };

  const [ptButton, setPtButton] = useState(false);

  const ButtonLanguage = ptButton ? (
    <PtButton language={() => changeLanguage("pt")}></PtButton>
  ) : (
    <EnButton language={() => changeLanguage("en")}></EnButton>
  );

  return (
    <>
      <Nav {...props}>
        <NavLogo to="/">
          <a href="/">
            <img
              className="navLogo"
              src={logo}
              alt="logo"
              // style={{ width: "200px", cursor: "pointer" }}
            />
          </a>
        </NavLogo>
        <Bars onClick={showSidebar} />
        <div className={sidebar ? "fechaMenu active" : "fechaMenu" } onClick={showSidebar}></div>
        <div
          className={sidebar ? "nav-menu active" : "nav-menu"}
          style={{
            display: "flex",
            // justifyContent: "center",
            // flexDirection: "column",
          }}
        >
          <Link to="#" className="menu-close">
            <div
              style={{
                justifyContent: "flex-start",
                position: "fixed",
                alignItems: "flex-start",
              }}
            >
              <button
                style={{
                  border: "none",
                  color: "white",
                  backgroundColor: "transparent",
                  padding: "10px",
                  height: "10px",
                  width: "10px",
                }}
              >
                X
              </button>
            </div>
          </Link>
          <div style={{
              width: "100%",
              flexDirection:"column",
              display: "flex",
              justifyContent:"center", 
              alignContent:"center"}}>
          <div
            style={{
              width: "100%",

              display: "flex",
              justifyContent: "center",
              alignSelf: "center",
              flexDirection: "column",
              alignItems: "center",
              textAlign: "center",
            }}
          >
            
            <ul
              style={{ padding: "0px", height: "fit-content", margin: "0px" }}
            >
              <Link className="linkStyle" to="/">
                <li>HOME</li>
              </Link>
              {/* <Link className="linkStyle" to="/models/mestra240">
                <li>SHOP</li>
              </Link> */}
              <Link className="linkStyle" to="/contact">
                <li  style={{textTransform:"uppercase"}}>{t("contact")}</li>
              </Link>
              <Link className="linkStyle" to="/about">
                <li  style={{textTransform:"uppercase"}}>{t("about")}</li>
              </Link>
               
              <li>
                <div className="dropSideModels">
                  MODELOS
                  <div className="dropSideContent">
                    <ul>
                      <Link
                        className="linkStyle"
                        onClick={() => {
                          window.location.href = "/models/mestra240";
                        }}
                        to="/models/mestra240"
                      >
                        <li>M240</li>
                      </Link>
                      <Link
                        className="linkStyle"
                        onClick={() => {
                          window.location.href = "/models/mestra230";
                        }}
                        to="/models/mestra230"
                      >
                        {" "}
                        <li>M230</li>
                      </Link>
                      <Link
                        className="linkStyle"
                        onClick={() => {
                          window.location.href = "/models/mestra222";
                        }}
                        to="/models/mestra222"
                      >
                        {" "}
                        <li>M222</li>
                      </Link>
                      <Link
                        className="linkStyle"
                        onClick={() => {
                          window.location.href = "/models/mestra212";
                        }}
                        to="/models/mestra212"
                      >
                        {" "}
                        <li>M212</li>
                      </Link>
                      <Link
                        className="linkStyle"
                        onClick={() => {
                          window.location.href = "/models/mestra200";
                        }}
                        to="/models/mestra200"
                      >
                        {" "}
                        <li>M200</li>
                      </Link>
                      <Link
                        className="linkStyle"
                        onClick={() => {
                          window.location.href = "/models/mestra198";
                        }}
                        to="/models/mestra198"
                      >
                        {" "}
                        <li>M198</li>
                      </Link>
                      <Link
                        className="linkStyle"
                        onClick={() => {
                          window.location.href = "/models/mestra160";
                        }}
                        to="/models/mestra160"
                      >
                        {" "}
                        <li>M160</li>
                      </Link>
                    </ul>
                  </div>
                  
                </div>
              
              </li>
            </ul>
          </div>
          <div onClick={() => setPtButton(!ptButton)} style={{width:"100%",display:"flex", justifySelf:"flex-end", alignSelf:"flex-end", justifyContent:"center", alignContent:"center"}}>
          <div className="buttonMenuSidebar" style={{ width:"fit-content", display:"flex"}} >
               <div onClick={() => changeLanguage()}>{ButtonLanguage}</div></div>
        </div>
        </div>
        </div>
        <NavMenu>
          <NavMenus />
        </NavMenu>

        <NavRight>
          {/* <NavLink to="/" activeStyle>
            SHOP */}
          {/* <Button
              style={{ backgroundColor: "transparent" }}
              animated="vertical"
            >
              <Button.Content hidden>SHOP</Button.Content>
              <Button.Content visible>
                <Icon name="shop" />
              </Button.Content>
            </Button> */}
          {/* </NavLink> */}
          {/* <NavLink to="/" activeStyle>
            LOGIN
          </NavLink> */}
          <div className="menuDropDown" onClick={showMenuSidebar}>
            <div style={{cursor:"pointer"}}>MENU</div>
            <div className={menuSidebar ? "fechaMenu active" : "fechaMenu"} onClick={showMenuSidebar}></div>
            <div 
              className={menuSidebar ? "navbar-menu active" : "navbar-menu"}
              style={{
                display: "flex",
              }}
            > 
              <Link to="#" className="menu-close">
                <div
                  style={{
                    justifyContent: "flex-start",
                    position: "fixed",
                    alignItems: "flex-start",
                  }}
                >
                  <button
                    style={{
                      border: "none",
                      color: "black",
                      backgroundColor: "transparent",
                      padding: "10px",
                      height: "10px",
                      width: "10px",
                    }}
                  >
                    X
                  </button>
                </div>
              </Link>
              <ul>
                <li>Local</li>

                <Link
                  to="/contact"
                  onClick={() => {
                    window.location.href = "/contact";
                  }}
                >
                  <li>Contato</li>
                </Link>
                <Link
                  to="/about"
                  onClick={() => {
                    window.location.href = "/about";
                  }}
                >
                  <li>Empresa</li>
                </Link>
                {/*                
                <li>Encontre uma revenda</li>
                <li>Pós Venda</li>
                <li>Marque sua revisao</li> */}
              </ul>
            </div>
          </div>
          <div onClick={() => setPtButton(!ptButton)}>{ButtonLanguage}</div>
        </NavRight>
      </Nav>
    </>
  );
};

export default Navbar;
