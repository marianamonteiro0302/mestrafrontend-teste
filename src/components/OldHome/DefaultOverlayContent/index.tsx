import React from 'react';
import { useTranslation } from 'react-i18next';
import { Container, Heading, Buttons } from './styles';
import { IoIosArrowDown } from 'react-icons/io';
import { makeStyles } from '@material-ui/core';
interface Props {
  label: string;
  description: string;
  modelNameURL: string;
  videoContainer: boolean;
}
const useStyles = makeStyles({
  arrowDownDiv: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    paddingBottom: '20px',
    bottom: '20px',
  },

  arrowDownIcon: {
    animation: 'animateDown infinite 1.5s',
    height: '30px',
    width: '30px',
  },
  buttonArrow: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
const DefaultOverlayContent: React.FC<Props> = ({
  label,
  description,
  modelNameURL,
  videoContainer,
}) => {
  const classes = useStyles();
  const { t, i18n } = useTranslation();
  const navigate = () => {
    if (modelNameURL === 'mestra322' || modelNameURL === 'mestra292') {
      window.location.href = `/models/${modelNameURL}`;
    } else window.location.href = `/lanchas/${modelNameURL}`;
  };
  return (
    <Container
      style={{
        position: videoContainer ? 'fixed' : 'inherit',
        overflow: videoContainer ? 'hidden' : 'inherit',
      }}
    >
      {videoContainer ? (
        false
      ) : (
        <>
          <Heading>
            <h1
              style={{ fontWeight: 'bold', cursor: 'pointer' }}
              onClick={() => navigate()}
            >
              {label}
            </h1>
          </Heading>
          <div className={classes.buttonArrow}>
            <Buttons>
              {modelNameURL === 'mestra322' || modelNameURL === 'mestra292' ? (
                <button
                  onClick={() =>
                    (window.location.href = `/models/${modelNameURL}`)
                  }
                >
                  Model Page
                </button>
              ) : (
                <button
                  onClick={() =>
                    (window.location.href = `/lanchas/${modelNameURL}`)
                  }
                >
                  {' '}
                  {t('customizar')}
                </button>
              )}
              <button
                className="white"
                onClick={() => (window.location.href = `/contact`)}
              >
                {t('solicitarOrcamento')}
              </button>
            </Buttons>
            {/* {label === "Mestra 240" ? (
      <div className={classes.arrowDownDiv}>
        <IoIosArrowDown className={classes.arrowDownIcon} />
      </div>
    ) : null} */}
          </div>
        </>
      )}
    </Container>
  );
};

export default DefaultOverlayContent;
