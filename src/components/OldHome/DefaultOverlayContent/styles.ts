import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  position: sticky;
  // height:100vh;
  z-index: 1000;
  flex-direction: column;
  justify-content: space-between;
  flex: 1;
  @media (max-width: 900px) {
    padding-bottom: 20px;
  }
`;

export const Heading = styled.div`
  margin-top: 16.5vh;
  width: 100%;
  text-align: center;

  > h1 {
    font-weight: 500;
    font-size: 40px;
    line-height: 48px;
    color: black;
  }
  > h2 {
    font-weight: normal;
    font-size: 14px;
    line-height: 20px;
    color: #5c5e62;
  }
`;

export const Buttons = styled.div`
  display: flex;
  // flex-direction: column;
  justify-content: center;
  align-items: center;
  gap: 20px;
  margin-bottom: 100px;
  z-index: 10;
  > button {
    background-color: black;
    color: #fff;
    opacity: 0.85;
    width: 260px;
    font-size: 12px;
    font-weight: 500;
    letter-spacing: 0.4px;
    text-transform: uppercase;

    padding: 13px 40px;
    border-radius: 20px;
    border: none;
    outline: 0;
    cursor: pointer;

    &.white {
      background: white;
      color: #1a1720;
    //   opacity: 0.65;

    }

    // & + button {
    //   margin: 10px 0 0;
    // }
  }

  @media (max-width: 600px) {
    flex-direction: column;
    margin-bottom: 80px;
    gap: 10px;

    > button + button {
      margin: 10px 0 0 0px;
    }
  }
  @media (max-width: 380px) {
    flex-direction: column;
    margin-bottom: 80px;

    > button + button {
      margin: 5px 0 0 10px;
    }
  }
    @media only screen and (width:810px) and (height:1080px) and (orientation:portrait) {
      margin-bottom: 100px;
    }
 
      @media only screen and (width: 810px) and (height:1080px) and (orientation:landscape){
        flex-direction: column;
        margin-bottom: 100px;
        > button + button {
          margin: 0 0 0 10px;
        }
      }
     
    }
`;
