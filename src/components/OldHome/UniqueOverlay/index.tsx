import React from 'react';
import { useTransform } from 'framer-motion';
import Box from '@material-ui/core/Box';
import useWrapperScroll from '../useWrapperScroll';
import { Button, Toolbar } from '@mui/material';
import { makeStyles } from '@material-ui/styles';
import { Container, Header, Footer, LastSection } from './styles';
import { useTranslation } from 'react-i18next';
import theme from '../../../theme/Theme';
import NewNavbar from '../../NewNavbar/NewNavbar';
import { ClassNames } from '@emotion/react';
import { useMediaQuery } from '@material-ui/core';
import VideoContainer from '../../Home/VideoSection/VideoContainer';

const useStyles = makeStyles({
  footer: {
    width: '100%',
    backgroundColor: 'white',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    // //mobile
    // [theme.breakpoints.down("md")]: {
    //   backgroundColor: "white",
    //   display: "flex",
    //   justifyContent: "center",
    //   alignItems: "center",
    // },
  },
});

const UniqueOverlay: React.FC = () => {
  const classes = useStyles();
  const { scrollYProgress } = useWrapperScroll();
  const opacity = useTransform(scrollYProgress, [0.9, 1], [0, 100]);
  const { t, i18n } = useTranslation();
  const footerMobileVersion = useMediaQuery(theme.breakpoints.down('md'));
  const showInMapClicked = () => {
    window.open(
      'https://www.google.com/maps/place/Mestra+Boats/@-22.3323056,-48.7627983,1069m/data=!3m1!1e3!4m14!1m8!3m7!1s0x0:0x0!2zMjLCsDE5JzU2LjMiUyA0OMKwNDUnNDEuMSJX!3b1!7e2!8m2!3d-22.3323184!4d-48.7614203!3m4!1s0x94bf5578da2609ab:0xcd23939e02babd7a!8m2!3d-22.3326358!4d-48.7615203'
    );
  };
  // console.log('footerMobile', footerMobileVersion);
  return (
    <Container>
      <Header>
        <NewNavbar />
      </Header>
      <LastSection style={{ opacity }}>
        <VideoContainer />
      </LastSection>
      <hr style={{ background: 'transparent', color: 'transparent' }} />
      <hr style={{ background: 'transparent', color: 'transparent' }} />
      <hr style={{ background: 'transparent', color: 'transparent' }} />
      <hr style={{ background: 'transparent', color: 'transparent' }} />

      {/* {footerMobileVersion ? <div style={{width:"100%", height:"150px", backgroundColor:"transparent"}}></div> : null}  */}
      <Footer style={{ opacity }}>
        <div className={classes.footer}>
          <Button onClick={() => (window.location.href = '/')}>
            Mestra © 2022
          </Button>
          <Button onClick={() => (window.location.href = '/about')}>
            {t('about')}
          </Button>
          <Button onClick={() => (window.location.href = '/contact')}>
            {t('contact')}
          </Button>
          <Button
            onClick={() => {
              showInMapClicked();
            }}
          >
            {t('local')}
          </Button>
        </div>
      </Footer>
    </Container>
  );
};

export default UniqueOverlay;
