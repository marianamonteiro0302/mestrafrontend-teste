import styled from 'styled-components';
import { motion } from 'framer-motion';

export const Container = styled.div`
  // height: 100vh;
  overflow-y: hidden;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
`;

export const Header = styled.header`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  display: flex;
  justify-content: space-between;
  align-items: center;
  -webkit-transform: translateZ(0);
  // padding: 0 20px;
  min-height: 52px;
`;

export const LastSection = styled(motion.div)`
  background-color: transparent;
  width: 100%;
  display: flex;
  justify-self: flex-end;
  justify-content: center;
  align-items: center;
  bottom: 0;
  left: 0;
  right: 0;
`;

export const Footer = styled(motion.footer)`
  background-color: transparent;
  position: fixed;
  width: 100%;
  display: flex;
  justify-self: flex-end;
  justify-content: center;
  align-items: center;
  bottom: 0;
  left: 0;
  right: 0;
`;
