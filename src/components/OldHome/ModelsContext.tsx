import React, { ReactNode } from 'react';

export interface LanchaModel {
  modelName: string;
  overlayNode: ReactNode;
  sectionRef: React.RefObject<HTMLElement>;
}

interface ModelsContext {
  wrapperRef: React.RefObject<HTMLElement>;
  registeredModels: LanchaModel[];
  registerModel: (model: LanchaModel) => void;
  unregisterModel: (modelName: string) => void;
  getModelByName: (modelName: string) => LanchaModel | null;
}

// Export with default values
export default React.createContext<ModelsContext>({} as ModelsContext);
