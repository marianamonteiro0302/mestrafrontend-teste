import styled from 'styled-components';

//imagens
//desktop
// import home322 from '../../data/images/desktop/mestra322_3.jpg';
// import home322 from '../../data/images/MESTRA322/mestra322-home.png';
import home322 from '../../data/images/MESTRA322/V3-Lancha-322-Home.png';
// import home292 from '../../data/images/MESTRA292/mestra292-home.png';
import home292 from '../../data/images/MESTRA292/V3-Lancha-292-Home.png';

import home240 from '../../data/images/desktop/home240_desktop.jpg';
import home230 from '../../data/images/desktop/home230_desktop.jpg';
import home200 from '../../data/images/desktop/home200_desktop.jpg';
import home212 from '../../data/images/desktop/home212_desktop.jpg';
import home160 from '../../data/images/desktop/home160_desktop.jpg';
import home180 from '../../data/images/desktop/home180_desktop.jpg';
//mobile
import home292Mobile from '../../data/images/mobile/Mobile-Lancha-292-Home.png';
import home322Mobile from '../../data/images/mobile/Mobile-Lancha-322-Home.png';
import home160Mobile from '../../data/images/mobile/home160_mobile_v01.jpg';
import home200Mobile from '../../data/images/mobile/home_200_mobile_v01.jpg';
import home212Mobile from '../../data/images/mobile/home212_mobile_v01.jpg';
import home230Mobile from '../../data/images/mobile/home230_nobile_v01.jpg';
import home240Mobile from '../../data/images/mobile/home240_mobile_v01.jpg';
import home180Mobile from '../../data/images/mobile/home180_mobile_v01.jpg';

export const Container = styled.div`
  .colored:nth-child(1) {
    background-image: url(${home322});
    // background: linear-gradient(
    //     0deg,
    //     rgba(79, 129, 136, 0) 60%,
    //     rgba(131, 174, 188, 1) 100%
    //   ),
    //   url(${home322});
    background-size: cover;
    background-position: center center;
    @media (max-width: 1024px) {
      background-size: cover;
      background-repeat: no-repeat;
      background-image: url(${home322Mobile});
      background-position: center center;
    }
  }
  .colored:nth-child(2) {
    background-image: url(${home292});
    // background: linear-gradient(
    //     0deg,
    //     rgba(79, 129, 136, 0) 60%,
    //     rgba(131, 174, 188, 1) 100%
    //   ),
    //   url(${home292});
    background-size: cover;
    background-position: center center;
    @media (max-width: 1024px) {
      background-size: cover;
      background-repeat: no-repeat;
      background-image: url(${home292Mobile});
      background-position: center center;
    }
  }
  .colored:nth-child(3) {
    background-image: url(${home240});
    // background: linear-gradient(
    //     0deg,
    //     rgba(79, 129, 136, 0) 60%,
    //     rgba(131, 174, 188, 1) 100%
    //   ),
    //   url(${home240});
    background-size: cover;
    background-position: center center;
    @media (max-width: 1024px) {
      background-size: cover;
      background-repeat: no-repeat;
      background-image: url(${home240Mobile});
      background-position: center center;
    }
  }
  .colored:nth-child(4) {
    background-position: center center;
    background-image: url(${home230});
    background-repeat: no-repeat;
    background-size: cover;
    @media (max-width: 1024px) {
      background-image: url(${home230Mobile});
      background-position: center center;
    }
  }
  .colored:nth-child(5) {
    background-position: center center;
    background-image: url(${home200});
    background-repeat: no-repeat;
    background-size: cover;
    @media (max-width: 1024px) {
      background-image: url(${home200Mobile});
      background-position: center center;
    }
  }
  .colored:nth-child(6) {
    background-position: center center;
    background-image: url(${home212});
    background-repeat: no-repeat;
    background-size: cover;
    @media (max-width: 1024px) {
      background-image: url(${home212Mobile});
      background-position: center center;
    }
  }
  .colored:nth-child(7) {
    background-position: center center;
    background-image: url(${home180});
    background-repeat: no-repeat;
    background-size: cover;
    @media (max-width: 1024px) {
      background-image: url(${home180Mobile});
      background-position: center center;
    }
  }
  .colored:nth-child(8) {
    background-position: center center;
    background: linear-gradient(
        180deg,
        rgba(215, 216, 210, 1) 0%,
        rgba(191, 195, 191, 0) 28%,
        rgba(173, 180, 178, 0) 48%,
        rgba(127, 141, 144, 0) 100%
      ),
      url(${home160});
    background-repeat: no-repeat;
    background-size: cover;
    @media (max-width: 1024px) {
      background-image: url(${home160Mobile});
      background-position: center center;
    }
  }
`;

export const Spacer = styled.div`
  height: 15vh;
  background: white;

  }
`;
