import styled from 'styled-components';

export const Container = styled.div`
  height: 100vh;
  scroll-snap-type: y mandatory;
  overflow-y: scroll;
  
  ::-webkit-scrollbar-button {
    height: 12px;
  }
  ::-webkit-scrollbar {
      width: 12px;
  }
  
  /* Track */
  ::-webkit-scrollbar-track {
      -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
      -webkit-border-radius: 10px;
      border-radius: 10px;
  }
  /* Handle */
  ::-webkit-scrollbar-thumb {
      -webkit-border-radius: 10px;
      border-radius: 10px;
      background: gray
      -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5);
  }

  @media (max-width: 900px) {
    overflow: overlay;
    ::-webkit-scrollbar {
      background: transparent;
      width: 4px;
    }
    ::-webkit-scrollbar-thumb {
      background: transparent;
    }
  }
`;

export const OverlaysRoot = styled.div`
  position: sticky;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
`;
