import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';
import { Grid, Button, Typography } from '@mui/material';
import theme from '../../theme/Theme';
import { useMediaQuery } from '@material-ui/core';
import { useAnimation, motion } from 'framer-motion';
import {
  Animator,
  ScrollContainer,
  ScrollPage,
  batch,
  Fade,
  FadeIn,
  Move,
  MoveIn,
  MoveOut,
  Sticky,
  StickyIn,
  ZoomIn,
} from 'react-scroll-motion';
import { GiFuelTank } from 'react-icons/gi';
import { GiBoatEngine } from 'react-icons/gi';
import { MdMeetingRoom } from 'react-icons/md';
import { GiWeight } from 'react-icons/gi';
import { IoIosPeople } from 'react-icons/io';
import { MdDirectionsBoat } from 'react-icons/md';
import { useInView } from 'react-intersection-observer';
import { useTranslation } from 'react-i18next';
const useStyles = makeStyles({
  root: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    gap: '250px',
    color: 'white',
    //mobile
    [theme.breakpoints.down('md')]: {
      gap: '5px',
      padding: '20px',
    },
    // gap: "50px",
  },
  names: {
    //  backgroundColor: "green",
  },
  values: {
    backgroundColor: 'pink',
  },
  li: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    gap: '10px',
  },
});

const FichaContainer = (props) => {
  const { t, i18n } = useTranslation();
  const classes = useStyles();
  const textMobile = useMediaQuery(theme.breakpoints.down('md'));
  // const animationControl = useAnimation();
  // const { inView, entry, ref } = useInView();
  // console.log(inView);
  // if (inView) {
  //   animationControl.start({
  //     x: 0,
  //     transition: {
  //       delay: 0.7,
  //     },
  //   });
  // }
  return (
    <>
      <div
        className={classes.root}
        //  ref={ref}
      >
        <div className={classes.names}>
          {/* <motion.div
            initial={{
              x: "100vw",
            }}
            animate={animationControl}
            className="box"
          > */}
          <ul
            style={{
              display: 'flex',
              flexDirection: 'column',
              gap: '20px',
              listStyle: 'none',
            }}
          >
            <li className={classes.li}>
              <GiFuelTank style={{ width: ' 42px', height: ' 42px' }} />
              <Typography variant={textMobile ? 'body1' : 'h5'}>
                {t('combustivelTanque')}
              </Typography>
            </li>
            <li className={classes.li}>
              {/* <Typography variant={textMobile ? 'body1' : 'h5'}>
                {t('tanqueAguaDoce')}
              </Typography> */}
              {props.mestra240 ? (
                <GiFuelTank style={{ width: ' 42px', height: ' 42px' }} />
              ) : props.mestra230 ? (
                <GiFuelTank style={{ width: ' 42px', height: ' 42px' }} />
              ) : props.mestra222 ? (
                <GiFuelTank style={{ width: ' 42px', height: ' 42px' }} />
              ) : props.mestra212 ? (
                <GiFuelTank style={{ width: ' 42px', height: ' 42px' }} />
              ) : props.mestra200 ? (
                <GiFuelTank style={{ width: ' 42px', height: ' 42px' }} />
              ) : props.mestra198 ? (
                <GiFuelTank style={{ width: ' 42px', height: ' 42px' }} />
              ) : props.mestra180 ? (
                <GiFuelTank style={{ width: ' 42px', height: ' 42px' }} />
              ) : props.mestra160 ? (
                ''
              ) : null}
            </li>
            <li className={classes.li}>
              <GiWeight style={{ width: ' 42px', height: ' 42px' }} />
              <Typography variant={textMobile ? 'body1' : 'h5'}>
                {t('pesoSemMotor')}
              </Typography>
            </li>
            <li className={classes.li}>
              <IoIosPeople style={{ width: ' 42px', height: ' 42px' }} />
              <Typography variant={textMobile ? 'body1' : 'h5'}>
                {t('pessoas')}
              </Typography>
            </li>
            <li className={classes.li}>
              {props.mestra240 ? (
                <MdMeetingRoom style={{ width: ' 42px', height: ' 42px' }} />
              ) : props.mestra230 ? (
                <MdMeetingRoom style={{ width: ' 42px', height: ' 42px' }} />
              ) : props.mestra222 ? (
                ''
              ) : props.mestra212 ? (
                ''
              ) : props.mestra200 ? (
                ''
              ) : props.mestra198 ? (
                ''
              ) : props.mestra180 ? (
                ''
              ) : props.mestra160 ? (
                ''
              ) : null}
              <Typography variant={textMobile ? 'body1' : 'h5'}>
                {props.mestra240
                  ? t('camarotes')
                  : props.mestra230
                  ? t('camarotes')
                  : props.mestra222
                  ? ''
                  : props.mestra212
                  ? ''
                  : props.mestra200
                  ? ''
                  : props.mestra198
                  ? ''
                  : props.mestra180
                  ? ''
                  : props.mestra160
                  ? ''
                  : null}
              </Typography>
            </li>
            <li className={classes.li}>
              {props.mestra240 ? (
                <MdDirectionsBoat style={{ width: ' 42px', height: ' 42px' }} />
              ) : props.mestra230 ? (
                <MdDirectionsBoat style={{ width: ' 42px', height: ' 42px' }} />
              ) : props.mestra222 ? (
                ''
              ) : props.mestra212 ? (
                ''
              ) : props.mestra200 ? (
                ''
              ) : props.mestra198 ? (
                ''
              ) : props.mestra180 ? (
                ''
              ) : props.mestra160 ? (
                ''
              ) : null}

              <Typography variant={textMobile ? 'body1' : 'h5'}>
                {props.mestra240
                  ? t('peDireitoCabine')
                  : props.mestra230
                  ? t('peDireitoCabine')
                  : props.mestra222
                  ? ''
                  : props.mestra212
                  ? ''
                  : props.mestra200
                  ? ''
                  : props.mestra198
                  ? ''
                  : props.mestra180
                  ? ''
                  : props.mestra160
                  ? ''
                  : null}
              </Typography>
            </li>
            <li className={classes.li}>
              <GiBoatEngine style={{ width: ' 42px', height: ' 42px' }} />
              <Typography variant={textMobile ? 'body1' : 'h5'}>
                {t('motorizacaoCentroRabeta')}
              </Typography>
            </li>
          </ul>
          {/* </motion.div> */}
        </div>

        <div className={classes.names}>
          {/* <motion.div
            initial={{
              x: "100vw",
            }}
            animate={animationControl}
            className="box"
          > */}
          <ul
            style={{
              display: 'flex',
              flexDirection: 'column',
              gap: '20px',
              listStyle: 'none',
            }}
          >
            <li className={classes.li}>
              {props.mestra240 ? (
                <div style={{ width: ' 42px', height: ' 42px' }}></div>
              ) : props.mestra230 ? (
                <div style={{ width: ' 42px', height: ' 42px' }}></div>
              ) : props.mestra222 ? null : props.mestra212 ? null : props.mestra200 ? null : props.mestra198 ? null : props.mestra180 ? null : props.mestra160 ? null : null}
              <Typography variant={textMobile ? 'body1' : 'h5'}>
                {props.mestra240
                  ? '19°'
                  : props.mestra230
                  ? '19°'
                  : props.mestra222
                  ? null
                  : props.mestra212
                  ? null
                  : props.mestra200
                  ? null
                  : props.mestra198
                  ? null
                  : props.mestra180
                  ? null
                  : props.mestra160
                  ? null
                  : null}
              </Typography>
            </li>
            <li className={classes.li}>
              <div style={{ width: ' 42px', height: ' 42px' }}></div>
              <Typography variant={textMobile ? 'body1' : 'h5'}>
                {props.mestra240
                  ? '192 L'
                  : props.mestra230
                  ? '192 L'
                  : props.mestra222
                  ? '100 L'
                  : props.mestra212
                  ? '100 L'
                  : props.mestra200
                  ? '100 L'
                  : props.mestra198
                  ? '100 L'
                  : props.mestra180
                  ? '100 L'
                  : props.mestra160
                  ? '43 L'
                  : null}
              </Typography>
            </li>
            <li className={classes.li}>
              <div style={{ width: ' 42px', height: ' 42px' }}></div>
              <Typography variant={textMobile ? 'body1' : 'h5'}>
                {props.mestra240
                  ? '60 L'
                  : props.mestra230
                  ? '60 L'
                  : props.mestra222
                  ? '60 L'
                  : props.mestra212
                  ? '60 L'
                  : props.mestra200
                  ? '45 L'
                  : props.mestra198
                  ? '28 L'
                  : props.mestra180
                  ? '28 L'
                  : props.mestra160
                  ? ''
                  : null}
              </Typography>
            </li>
            <li className={classes.li}>
              <div style={{ width: ' 42px', height: ' 42px' }}></div>
              <Typography variant={textMobile ? 'body1' : 'h5'}>
                {props.mestra240
                  ? '1150 KG'
                  : props.mestra230
                  ? '1150 KG'
                  : props.mestra222
                  ? '900 KG'
                  : props.mestra212
                  ? '700 KG'
                  : props.mestra200
                  ? '550 KG'
                  : props.mestra198
                  ? '550 KG'
                  : props.mestra180
                  ? '485 KG'
                  : props.mestra160
                  ? '485 KG'
                  : null}
              </Typography>
            </li>
            <li className={classes.li}>
              <div style={{ width: ' 42px', height: ' 42px' }}></div>

              <Typography variant={textMobile ? 'body1' : 'h5'}>
                {props.mestra240
                  ? '1 + 9'
                  : props.mestra230
                  ? '8 dia / 2 noite'
                  : props.mestra222
                  ? '1+9'
                  : props.mestra212
                  ? '1 + 8'
                  : props.mestra200
                  ? '1+7'
                  : props.mestra198
                  ? '1+7'
                  : props.mestra180
                  ? '1+7'
                  : props.mestra160
                  ? '1+3'
                  : null}
              </Typography>
            </li>
            <li className={classes.li}>
              {props.mestra240 ? (
                <div style={{ width: ' 42px', height: ' 42px' }}></div>
              ) : props.mestra230 ? (
                <div style={{ width: ' 42px', height: ' 42px' }}></div>
              ) : props.mestra222 ? null : props.mestra212 ? null : props.mestra200 ? null : props.mestra198 ? null : props.mestra180 ? null : props.mestra160 ? null : null}
              <Typography variant={textMobile ? 'body1' : 'h5'}>
                {props.mestra240
                  ? '1'
                  : props.mestra230
                  ? '1'
                  : props.mestra222
                  ? ''
                  : props.mestra212
                  ? ''
                  : props.mestra200
                  ? ''
                  : props.mestra198
                  ? ''
                  : props.mestra180
                  ? ''
                  : props.mestra160
                  ? ''
                  : null}
              </Typography>
            </li>
            <li className={classes.li}>
              {props.mestra240 ? (
                <div style={{ width: ' 42px', height: ' 42px' }}></div>
              ) : props.mestra230 ? (
                <div style={{ width: ' 42px', height: ' 42px' }}></div>
              ) : props.mestra222 ? null : props.mestra212 ? null : props.mestra200 ? null : props.mestra198 ? null : props.mestra180 ? null : props.mestra160 ? null : null}
              <Typography variant={textMobile ? 'body1' : 'h5'}>
                {props.mestra240
                  ? '1,45 M'
                  : props.mestra230
                  ? '1,45 M'
                  : props.mestra222
                  ? ''
                  : props.mestra212
                  ? ''
                  : props.mestra200
                  ? ''
                  : props.mestra198
                  ? ''
                  : props.mestra180
                  ? ''
                  : props.mestra160
                  ? ''
                  : null}
              </Typography>
            </li>
            <li className={classes.li}>
              <div style={{ width: ' 42px', height: ' 42px' }}></div>
              <Typography variant={textMobile ? 'body1' : 'h5'}>
                {props.mestra240
                  ? ' de 200 à 280 HP'
                  : props.mestra230
                  ? ' de 200 à 280 HP'
                  : props.mestra222
                  ? 'de 200 à 280 HP'
                  : props.mestra212
                  ? ' de 100 à 120 HP'
                  : props.mestra200
                  ? ' de 90 à 150 HP'
                  : props.mestra198
                  ? ' de 90 à 150 HP'
                  : props.mestra180
                  ? ' de 60 à 135 HP'
                  : props.mestra160
                  ? ' de 40 à 60 HP'
                  : null}
              </Typography>
            </li>
          </ul>
          {/* </motion.div> */}
        </div>
      </div>
    </>
  );
};

export default FichaContainer;
